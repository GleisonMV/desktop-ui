const { app, BrowserWindow } = require('electron')
const path = require('path')

function createWindow() {
  let win = new BrowserWindow({
    width: 800,
    height: 600,
    webPreferences: {
      nodeIntegration: true
    },
    frame: false
  })
  win.loadFile(path.join(__dirname, 'index.html'))
}

module.exports = function () {
  app.whenReady().then(createWindow)
}